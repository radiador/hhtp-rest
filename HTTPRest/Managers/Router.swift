//
//  Router.swift
//  HTTPRest
//
//  Created by formador on 26/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

final class Router {
    
    enum Routes {
        case photoListViewControllerRoute
    }
    
    static func launchRoute(route: Routes, from viewController: UIViewController) {
        
        switch route {
        case .photoListViewControllerRoute:
            launchPhotoListViewControllerRoute(from: viewController)
        }
        
    }
    
    
    static private func launchPhotoListViewControllerRoute(from viewController: UIViewController) {
        
        let photoListViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "photoListViewControllerIdentifier")
        
        viewController.navigationController?.pushViewController(photoListViewController, animated: true)
    }
    
}
