//
//  DataManager.swift
//  HTTPRest
//
//  Created by formador on 25/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

final class DataManager {
    
    static let shared = DataManager()
    
    private init() {}
    
    
    func loadPhotos(success: @escaping ([Photo]?) -> Void, errorHandler: @escaping (_ errorDescription: String) -> Void) {
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host = "jsonplaceholder.typicode.com"
        urlComponents.path = "/photos"
        
        guard let url = urlComponents.url else { return }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            
            DispatchQueue.main.async {
                if let error = error {
                    errorHandler(error.localizedDescription)
                    return
                }
                
                if let data = data {
                    let photos = try? JSONDecoder().decode([Photo].self, from: data)
                    
                    success(photos)
                    return
                }
                
                success(nil)
            }
            
        }.resume()
    }
    
    
    
    
    //A efectos de demo de como llamar a servideores
    func loadPost() {
        
        let urlServer = URL(string: "http://jsonplaceholder.typicode.com/posts/1")
        
        //Se puede crear crear y configurar un URLSession
        let urlConfiguration = URLSessionConfiguration.default
        let urlSession = URLSession(configuration: urlConfiguration)
        
        urlSession.dataTask(with: urlServer!) { data, URLResponse, error in
        
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            if let data = data {
                
                let post = try? JSONDecoder().decode(Post.self, from: data)
                
                print(post!)
                //Parseo lo que necesito
            }
        }.resume()
    }
    
    func loadPostTwo() {
        
        let urlServer = URL(string: "http://jsonplaceholder.typicode.com/posts/1")
        
        //Se puede usar el singleton por defecto de URLSession
        URLSession.shared.dataTask(with: urlServer!) { data, URLResponse, error in
            
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            if let data = data {
                
                let post = try? JSONDecoder().decode(Post.self, from: data)
                
                print(post!)
                //Parseo lo que necesito
            }
            }.resume()
    }
    
    func loadPostThree() {
        
        let urlServer = URL(string: "http://jsonplaceholder.typicode.com/posts/1")

        //Se puede usar URLRequest para configrar varios aspectos de la llamada como el metodo o los headers
        var urlRequest = URLRequest(url: urlServer!)
        urlRequest.httpMethod = "GET"
        
        //Se puede usar el singleton por defecto de URLSession
        URLSession.shared.dataTask(with: urlRequest, completionHandler: { data, URLResponse, error in
            
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            if let data = data {
                
                let post = try? JSONDecoder().decode(Post.self, from: data)
                
                print(post!)
                //Parseo lo que necesito
            }
            }).resume()
    }
}
