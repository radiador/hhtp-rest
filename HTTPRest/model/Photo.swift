//
//  Photo.swift
//  HTTPRest
//
//  Created by formador on 26/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

struct Photo: Decodable {
    
    let albumId: Int
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String
}
