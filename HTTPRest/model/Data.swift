//
//  Data.swift
//  HTTPRest
//
//  Created by formador on 25/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

struct Post: Codable {
    
    let userId: Int
    let id: Int
    let title: String
    let body: String
}
