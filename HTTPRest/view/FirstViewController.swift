//
//  FirstViewController.swift
//  HTTPRest
//
//  Created by formador on 25/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class FirstViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func loadPhotosButtonAction(_ sender: Any) {
        
        Router.launchRoute(route: .photoListViewControllerRoute, from: self)
    }
    
}

