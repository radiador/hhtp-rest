//
//  PhotoCollectionViewCell.swift
//  HTTPRest
//
//  Created by formador on 27/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
}
