//
//  SecondViewController.swift
//  HTTPRest
//
//  Created by formador on 25/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class PhotoListViewController: BaseViewController {

    @IBOutlet weak var imageCollectionViewController: UICollectionView!
    
    private var photos: [Photo]?
    
    private var downloadedPhotos = [Int: UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageCollectionViewController.register(UINib(nibName: "PhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "photoCellIdentifier")
        
        showLoading {
            self.loadPhotos()
        }
    }
    

    private func loadPhotos() {
        
        DataManager.shared.loadPhotos(success: { photos in
            
            self.hideLoading()
            
            self.photos = photos
            
            self.imageCollectionViewController.reloadData()
            
        }) { [weak self] error in
            
            let alertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil )
            
            alertController.addAction(okAction)
            
            self?.present(alertController, animated: true, completion: nil)
        }
    }
}


extension PhotoListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCellIdentifier", for: indexPath) as? PhotoCollectionViewCell
        
        if let photos = photos, let cell = cell {
            let photo = photos[indexPath.row]
            
            let image = downloadedPhotos[photo.id]
            
            if let image = image {
            
                cell.photoImageView.image = image
            } else {
                
                cell.photoImageView.image = nil
 
                if let url = URL(string: photo.url) {
                    URLSession.shared.dataTask(with: url) { data, _, _ in
                        if let data = data {
                            let image = UIImage(data: data)
                            self.downloadedPhotos[photo.id] = image
                            
                            DispatchQueue.main.async {
                                collectionView.reloadData()
                            }
                        }
                        }.resume()
                }
            }
        }
        
        return cell ?? PhotoCollectionViewCell()
    }
}
