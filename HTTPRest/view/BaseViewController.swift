//
//  BaseViewController.swift
//  HTTPRest
//
//  Created by formador on 26/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    //Funciona para que cualquier view controller que sea subclase de esta pueda mosyrar un loading
    func showLoading(completion: (() -> Void)? = nil) {
        
        //Instanciamos el loading
        let loadingViewController = LoadingViewController(nibName: "LoadingViewController", bundle: nil)
        
        //Presentamos modal
        present(loadingViewController, animated: true, completion: completion)
        
    }
    
    func hideLoading() {
        
        if let presentedViewController = self.presentedViewController, presentedViewController is LoadingViewController {
        
            dismiss(animated: true, completion: nil)
        }
    }
}
